-- | Functions that depend on the template-haskell version
module Data.Generics.Is.TH.Compat where

import Language.Haskell.TH

constructorNames :: Name -> Q [Name]
constructorNames datatype = do
  info <- reify datatype
  cs <- constructors info
  return$ cs >>= conNames
  where
    constructors ty = case ty of
        TyConI (DataD _ _ _ cs _) -> return cs
        TyConI (NewtypeD _ _ _ c _) -> return [c]
        _ -> fail $ "Can't derive predicates for " ++ nameBase datatype ++ "."

    conNames c = case c of
      NormalC n _    -> [n]
      RecC n _       -> [n]
      InfixC _ n _   -> [n]
      ForallC _ _ c' -> conNames c'
