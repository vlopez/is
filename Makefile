.PHONY: build install sdist

build:
	stack build

install:
	stack install

sdist:
	cabal sdist

# Testing
.PHONY: test test-all test-default test-%

test-all: test-default test-7.10.3 test-7.8.4 test-8.0.2 test-8.2.2 test-8.4.4

test: test-default

test-default:
	stack clean
	stack test
	stack haddock --no-haddock-deps

test-%:
	stack --stack-yaml stack-$*.yaml clean
	stack --stack-yaml stack-$*.yaml test
	stack --stack-yaml stack-$*.yaml haddock --no-haddock-deps

