module Data.Generics.Is {-# DEPRECATED "Import \"Data.Generics.Is.TH\" directly, or port your code to using \"Data.Generics.Is.Generic\"." #-} (
-- | Generate predicates from constructor names or from quoted patterns.
-- 
--   We provide three implementations of the functions @is@ and @isNot@, each
--   relying on a different generic programming technique.
-- 
--   [Template Haskell] "Data.Generics.Is.TH". Most flexible and efficient, works on GADTs.
--     Requires access to the type declaration; it can also declare predicate functions
--     from each constructor of a given type.
-- 
--   [Generics] "Data.Generics.Is.Generic". Safe. Requires an instance of 'GHC.Generics.Generic'.
--     It performs faster than SYB, according to <http://stackoverflow.com/a/17503441/2369398 this StackOverflow answer>.
-- 
--   [Scrap Your Boilerplate] "Data.Generics.Is.Data".
--     Safe. Works only on monomorphic values.
--     Requires an instance of 'Data.Data.Data'.

     module Data.Generics.Is.TH
     ) where

import Data.Generics.Is.TH


  
